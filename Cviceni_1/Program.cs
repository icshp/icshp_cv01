﻿using System;
using System.Net;
using System.IO;
using System.Xml;

namespace Cviceni_1
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                Console.WriteLine("\nVítejte v menu, zvolte si prosím jednu z možostí:");
                Console.WriteLine("1)Adresa\n2)Abeceda\n3)Rodné číslo\n4)Hádej číslo\n5)Počasí\n6)Konec");
                Console.WriteLine("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                Console.Write("Zadejte volbu:");
                try
                {
                    switch (Convert.ToInt32(Console.ReadLine()))
                    {
                        case 1:
                            Console.WriteLine("Úkol č.1 - Formátování adres:");
                            Address();
                            break;
                        case 2:
                            Console.WriteLine("Úkol č.2 - Abeceda:");
                            Alfabet();
                            break;
                        case 3:
                            Console.WriteLine("Úkol č.3 - Rodné číslo:\nZadejte rodné číslo:");
                            detector(Console.ReadLine());
                            break;
                        case 4:
                            Console.WriteLine("Úkol č.4 - Hádání čísla");
                            numberGame();
                            break;
                        case 5:
                            Console.WriteLine("Úkol č.5 - Počasí\nZadejte prosím město:");
                            try
                            {
                                weatherActu(Console.ReadLine());
                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Neplatné zadání!");
                            }
                            break;
                        case 6:
                            Environment.Exit(-1);
                            break; //Nikdy se sem nedostane!
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Neplatné zadání! {0}", e.ToString());
                }
            }
        }

        static void Address()
        {
            Console.WriteLine("Josef Novák\nJindřišská 16\n111 50, Praha 1");
        }

        static void Alfabet()
        {
            for (char i = 'a'; i <= 'z'; i++)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
        }

        static void detector(string rc)
        {
            try
            {
                int year = int.Parse(rc.Substring(0, 2));
                int month = int.Parse(rc.Substring(2, 2));
                int day = int.Parse(rc.Substring(4, 2));
                if (month - 50 > 0)
                {
                    Console.WriteLine("žena");
                }
                else
                {
                    Console.WriteLine("Muž");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Opakujte zadání!");
            }
        }

        static void numberGame()
        {
            Random random = new Random();
            int randomNumber = random.Next(0, 100);
            int iteration = 0;
            bool win = false;
            while (true)
            {
                iteration++;
                Console.Write("Zadejte číslo v rozmezí 0-100: ");
                int number = Convert.ToInt32(Console.ReadLine());
                if (number > randomNumber)
                {
                    Console.WriteLine("Číslo je větší, než hádané.");
                }
                else if (number < randomNumber)
                {
                    Console.WriteLine("Číslo je menší, než hádané.");
                }
                else
                {
                    Console.WriteLine("Gratuluji, vyhrál jsi! Tvoje: {0} vs hádané: {1}", number, randomNumber);
                    win = true;
                    break;
                }

                if (iteration >= 10)
                {
                    Console.WriteLine("Prohrál jsi, protože jsi na 10 pokus netrefil číslo");
                    break;
                }
            }
            if (win)
            {
                Console.WriteLine("Chceš hru opakovat? Y/N");
                string rerun = Console.ReadLine();
                if (rerun == "Y")
                {
                    numberGame();
                }

            }

        }

        static void weatherActu(string city)
        {
            WebClient client = new WebClient();
            Stream dataPosition = client.OpenRead("https://api.opencagedata.com/geocode/v1/xml?q=" + city + "&key=3eb0444e99af42408b67c82c8bcc42ae");
            StreamReader reader = new StreamReader(dataPosition);
            string textPosition = reader.ReadToEnd();

            XmlDocument docPosition = new XmlDocument();
            docPosition.LoadXml(textPosition);

            XmlNodeList elements = docPosition.GetElementsByTagName("geometry");
            var lat = elements[0].SelectSingleNode("lat").InnerText;
            var lng = elements[0].SelectSingleNode("lng").InnerText;
            Console.WriteLine("Souřadnice zadaného města/obce: lat:{0} lng:{1}", lat, lng);

            Stream data = client.OpenRead("https://api.met.no/weatherapi/locationforecast/1.9/?lat="+lat+"&lon="+lng);
            reader = new StreamReader(data);
            string textWeather = reader.ReadToEnd();

            data.Close();
            reader.Close();

            XmlDocument docWeather = new XmlDocument();
            docWeather.LoadXml(textWeather);

            XmlNodeList temperatureElement = docWeather.GetElementsByTagName("temperature");
            XmlNodeList windSpeedElement = docWeather.GetElementsByTagName("windSpeed");
            XmlNodeList pressureElement = docWeather.GetElementsByTagName("pressure");
            Console.WriteLine("Teplota {0}°C", temperatureElement[0].Attributes["value"].Value);
            Console.WriteLine("Rychlost větru {0} m/s", windSpeedElement[0].Attributes["mps"].Value);
            Console.WriteLine("Tlak {0} hPa", pressureElement[0].Attributes["value"].Value);
        }
    }
}
